<a href="#" id="back-to-top" title="Back to top">&uarr;</a>

<div id="calc" class="white-popup mfp-hide">

	<ul class="nav nav-pills nav-justified">
		<li role="presentation" class="tabs active"><a href="#Slabs" data-toggle="tab">Slabs, Footings, Walls</a></li>
  		<li role="presentation" class="tabs"><a href="#Columns" data-toggle="tab">Columns, Piles</a></li>
  		<li role="presentation" class="tabs"><a href="#Circular" data-toggle="tab">Circular Slab, Tube</a></li>
	</ul>


	<div class="tab-content">
	    <div role="tabpanel" class="tab-pane fade in active" id="Slabs">
	    	<form id="calc-slab" class="cc-form">
				<div class="row">
					<div class="col-xs-12 col-sm-2">
						<label>Length</label>
						<input type="text" class="form-control inputtop" id="s-Length" value="0">
						<select id="s-l-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-2">
						<label>Width</label>
						<input type="text" class="form-control inputtop" id="s-Width" value="0">
						<select id="s-w-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-2">
						<label>Height</label>
						<input type="text" class="form-control inputtop" id="s-Height" value="0">
						<select id="s-h-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-3">
						<label class="qlable">Quantity</label>
						<input type="text" class="form-control" id="s-Quantity" value="1">
					</div>

				</div>

				<div class="row">
					<div class="col-xs-12 txt-right">
					<span class="total">Total Volume: <span id="sb-total">0.00</span><span class="m3">m3</span></span>
					</div>
				</div>
			</form>

	    </div>

	    <div role="tabpanel" class="tab-pane" id="Columns">
	    	
	    	<form id="calc-columns" class="cc-form">
				<div class="row">
					<div class="col-xs-12 col-sm-2">
						<label>Diameter</label>
						<input type="text" class="form-control inputtop" id="c-Diameter" value="0">
						<select id="c-d-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-2">
						<label>Height</label>
						<input type="text" class="form-control inputtop" id="c-Height" value="0">
						<select id="c-h-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-3">
						<label class="qlable">Quantity</label>
						<input type="text" class="form-control" id="c-Quantity" value="1">
					</div>

				</div>

				<div class="row">
					<div class="col-xs-12 txt-right">
					<span class="total">Total Volume: <span id="cd-total">0.00</span><span class="m3">m3</span></span>
					</div>
				</div>
			</form>

	    </div>

	    <div role="tabpanel" class="tab-pane" id="Circular">
	    	<form id="calc-cslab" class="cc-form">
				<div class="row">
					<div class="col-xs-12 col-sm-2">
						<label>Outer Diameter</label>
						<input type="text" class="form-control inputtop" id="cs-ODiameter" value="0">
						<select id="cs-od-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-2">
						<label>Inner Diameter</label>
						<input type="text" class="form-control inputtop" id="cs-IDiameter" value="0">
						<select id="cs-id-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-2">
						<label class="hlabel">Height</label>
						<input type="text" class="form-control inputtop" id="cs-Height" value="0">
						<select id="cs-h-unit" class="form-control"><option value="foot">foot</option><option value="inch">inch</option><option value="yard">yard</option><option value="meter" selected="">meter</option><option value="centimeter">centimeter</option></select>
					</div>

					<div class="col-xs-12 col-sm-1"><span class="x">X</span></div>

					<div class="col-xs-12 col-sm-3">
						<label class="qlable">Quantity</label>
						<input type="text" class="form-control" id="cs-Quantity" value="1">
					</div>

				</div>

				<div class="row">
					<div class="col-xs-12 txt-right">
					<span class="total">Total Volume: <span id="cs-total">0.00</span><span class="m3">m3</span></span>
					</div>
				</div>
			</form>

	    </div>
	</div>
</div>





<footer class="footer-info">
  <div class="container">
  	<div class="row">
  		<div class="col-xs-12 col-sm-offset-2 col-sm-8">
  		&copy; <?php echo date("Y"); ?> <?php bloginfo('name'); ?>
  		</div>
  	</div>
  </div>
</footer>
