<?php global $mox_data; ?>
<header class="banner">
  <div class="container">
    <div class="row">
        <?php 
        if ( is_front_page() ) { ?>
          <div class="col-sm-4 col-md-5 hidden-xs txt-right">
            <a class="hd-nav contact-link" href="<?php echo $mox_data['mox-g-left-url']?>"><span>Calgary</span></a>
          </div>

          <div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4 col-md-2">
            <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
              <img class="img-responsive center-block" src="<?php echo $mox_data['mox-g-logo']['url']?>" alt="<?php bloginfo('name'); ?>">
            </a>
          </div>

          <div class="col-sm-4 col-md-5 hidden-xs">
            <a class="hd-nav quote-link" href="<?php echo $mox_data['mox-g-right-url']?>"><span>Edmonton</span></a>
          </div>

        <?php } else if (is_page_template( 'template-fourstar.php' )) { ?>
          <div class="col-sm-4 col-md-5 hidden-xs txt-right">
            <a class="hd-nav contact-link" href="#g2"><span>CONTACT US</span></a>
          </div>

          <div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4 col-md-2">
            <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
              <img class="img-responsive center-block" src="<?php echo $mox_data['mox-c-logo']['url']?>" alt="<?php bloginfo('name'); ?>">
            </a>
          </div>

          <div class="col-sm-4 col-md-5 hidden-xs">
            <a class="hd-nav quote-link" href="#g3"><span>REQUEST A QUOTE</span></a>
          </div>

        <?php } else if (is_page_template( 'template-edmonton.php' )) { ?>
          <div class="col-sm-4 col-md-5 hidden-xs txt-right">
            <a class="hd-nav contact-link" href="#g2"><span>CONTACT US</span></a>
          </div>

          <div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4 col-md-2">
            <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
              <img class="img-responsive center-block" src="<?php echo $mox_data['mox-e-logo']['url']?>" alt="<?php bloginfo('name'); ?>">
            </a>
          </div>

          <div class="col-sm-4 col-md-5 hidden-xs">
            <a class="hd-nav quote-link" href="#g3"><span>REQUEST A QUOTE</span></a>
          </div>

        <?php } else { ?>
          <div class="col-sm-4 col-md-5 hidden-xs txt-right">
            <a class="hd-nav contact-link" href="<?php echo $mox_data['mox-g-left-url']?>"><span>Calgary</span></a>
          </div>

          <div class="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-4 col-md-2">
            <a class="brand" href="<?= esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>">
              <img class="img-responsive center-block" src="<?php echo $mox_data['mox-g-logo']['url']?>" alt="<?php bloginfo('name'); ?>">
            </a>
          </div>

          <div class="col-sm-4 col-md-5 hidden-xs">
            <a class="hd-nav quote-link" href="<?php echo $mox_data['mox-g-right-url']?>"><span>Edmonton</span></a>
          </div>
        <?php } ?>
      </div>
    </div>
    
  </div>
</header>