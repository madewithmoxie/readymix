<div id="banner-group" class="container-fluid gofull">
 	<div class="row">
		<div id="single-image" style="background-image: url('<?php echo $mox_data['mox-e-banner']['url']?>');"></div>
	</div>
</div>
<div id="border-bot" class="gofull"></div>

<section class="home-brand-text">
	<div class="container txt-center">
		<div class="row">
			<div class="col-xs-12">
				<?php get_template_part('templates/page', 'header'); ?>
				<div class="alert alert-warning">
				  <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
