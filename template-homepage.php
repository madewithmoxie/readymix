<?php
/**
 * Template Name: Landing Page
 */
?>

<?php global $mox_data; ?>

<div id="banner-group" class="container-fluid gofull">
  <div class="row">
    <div id="banner-left" class="col-xs-12 col-md-6" style="background-image: url('<?php echo $mox_data['mox-g-left-banner']['url']?>');">
    	<a title="<?php echo $mox_data['mox-g-left-seo']?>" href="<?php echo $mox_data['mox-g-left-url']?>" class="btn main-btn">Calgary</a>
    </div>
    <div id="banner-right" class="col-xs-12 col-md-6" style="background-image: url('<?php echo $mox_data['mox-g-right-banner']['url']?>');">
    	<a title="<?php echo $mox_data['mox-g-right-seo']?>" href="<?php echo $mox_data['mox-g-right-url']?>" class="btn main-btn">Edmonton</a>
    </div>
  </div>
</div>

<div id="border-bot" class="gofull"></div>

<section class="home-brand-text">
	<div class="container txt-center">
		<div class="row">
			<div class="col-xs-12">
				<?php while (have_posts()) : the_post(); ?>
				  <?php get_template_part('templates/content', 'page'); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>
