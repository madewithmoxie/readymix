<?php
/**
 * Template Name: Edmonton Landing
 */
?>

<div id="banner-group" class="container-fluid gofull">
 	<div class="row">
		<div id="single-image" style="background-image: url('<?php echo $mox_data['mox-e-banner']['url']?>');"></div>
	</div>
</div>
<div id="border-bot" class="gofull"></div>

<section class="home-brand-text">
	<div class="container txt-center">
		<div class="row">
			<div class="col-xs-12">
				<?php while (have_posts()) : the_post(); ?>
				  <?php get_template_part('templates/content', 'page'); ?>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>


<section id="g2" class="home-brand-group">
	<div class="container text-center">
		<div class="row">
			<div class="col-xs-12 col-sm-6 border-right">
			<div class="svg-icon">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 42.84 46.59"><defs><style>.a{fill:none;stroke-linecap:round;stroke-linejoin:round;stroke-width:2px;stroke:url(#a);}</style><linearGradient id="a" x1="2.58" y1="24" x2="45.42" y2="24" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ed1c2f "/><stop offset="1" stop-color="#ed1c2f "/></linearGradient></defs><title>contact-svg</title><path class="a" d="M44.42,19c0,8.29-6.9,15.22-16.1,16.9C26.92,36.14,17.46,46.3,16,46.3c-3.15,0,1.87-10.63-.79-11.71C8.32,31.8,3.58,25.87,3.58,19,3.58,9.44,12.72,1.7,24,1.7S44.42,9.44,44.42,19ZM14.48,16.4a2,2,0,1,1-2,2A2,2,0,0,1,14.48,16.4Zm10,0a2,2,0,1,1-2,2A2,2,0,0,1,24.5,16.4Zm10,0a2,2,0,1,1-2,2A2,2,0,0,1,34.53,16.4Z" transform="translate(-2.58 -0.7)"/></svg>
			</div>
				<h4>Contact BURNCO Rock Products LTD</h4>
				<p><?php echo $mox_data['mox-e-contact']?></p>
					<h5>Order Desk <span class="phone"># <?php echo $mox_data['mox-e-order']?></span></h5>
					<h5>Main Number <span class="phone"># <?php echo $mox_data['mox-e-main']?></span></h5>
					<p><a href="#ccform" class="pop"><span>Email Us</span></a></p>
			</div>
			<div class="col-xs-12 col-sm-6">

				<div class="svg-icon">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 34.11 46.25"><defs><style>.a{fill:none;stroke-miterlimit:10;stroke:url(#a);}</style><linearGradient id="a" x1="7.5" y1="24.13" x2="41.61" y2="24.13" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ed1c2f "/><stop offset="1" stop-color="#ed1c2f "/></linearGradient></defs><title>caculator</title><path class="a" d="M38.9,46.75H10.21A2.21,2.21,0,0,1,8,44.54V3.71A2.21,2.21,0,0,1,10.21,1.5H38.9a2.21,2.21,0,0,1,2.21,2.21V44.54A2.21,2.21,0,0,1,38.9,46.75ZM8,13.64H41.11M13.52,19.16h4.41v4.41H13.52V19.16Zm0,8.83h4.41V32.4H13.52V28Zm0,8.83h4.41v4.41H13.52V36.82Zm8.83-17.66h4.41v4.41H22.35V19.16Zm0,8.83h4.41V32.4H22.35V28Zm0,8.83h4.41v4.41H22.35V36.82Zm8.83-17.66h4.41v4.41H31.18V19.16Zm0,8.83h4.41V32.4H31.18V28Zm0,8.83h4.41v4.41H31.18V36.82ZM26.76,5.91V9.23m4.41-3.31V9.23m4.41-3.31V9.23" transform="translate(-7.5 -1)"/></svg>
				</div>

				<h4>Concrete Calculator</h4>

				<p><?php echo $mox_data['mox-e-calc']?></p>
				
				<p><a href="#calc" class="pop"><span>Launch Calculator</span></a></p>
			</div>
		</div>
	</small>
	</div>
</section>


<section id="g3" class="row home-brand-quote">
	<div class="container text-center">
		<div class="row">
			<div class="col-sm-offset-2 col-xs-12 col-sm-8">
			<h2>Request A Quote</h2>
			<p><?php echo $mox_data['mox-e-form']?></p>
				<?php gravity_form('Request a Quote-E', false, false, false, '', true); ?>
			</div>
		</div>
	</div>
</section>

<div id="ccform" class="white-popup mfp-hide">
	<h3 class="txt-center">Contact Us</h3>
	<?php gravity_form('Contact Form-E', false, false, false, '', true); ?>
</div>