/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.



jQuery(document).ready(function($) {


if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function () {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function () {
        backToTop();
    });
    $('#back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}





// Popup

$('.pop').magnificPopup({
  type:'inline',
});


$('.hd-nav').click(function(){
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});


});


// Clear Searchbox #Search on focus
jQuery(document).ready(function($) {
    $.fn.Clearinput = function(){
        return $(this).each(function(){
            var Input = $(this);
            var default_value = Input.val();

            Input.focus(function() {
               if(Input.val() === default_value) { Input.val(""); }
            }).blur(function(){
                if(Input.val().length === 0) { Input.val(default_value); }
            });
        });
    };

    $('.form-control').Clearinput();

});



// Calculator for Slabs
jQuery(document).ready(function($) {

$( ".form-control" )
  .change(function () {
    var S_L_M = $("#s-l-unit").val();
    var S_W_M = $("#s-w-unit").val();
    var S_H_M = $("#s-h-unit").val();
    
    var S_L = $("#s-Length").val();
    var S_W = $("#s-Width").val();
    var S_H = $("#s-Height").val();
    
    var S_Q = $("#s-Quantity").val();
    
    if (S_L_M === "foot") {
        S_L = S_L * 0.3048;
    } else {
        if (S_L_M === "inch") {
            S_L = S_L * 0.0254;
        } else {
            if (S_L_M === "yard") {
                S_L = S_L * 0.9144;
            } else {
                if (S_L_M === "centimeter") {
                    S_L = S_L * 0.01;
                }
            }
        }
    }
    
    if (S_W_M === "foot") {
        S_W = S_W * 0.3048;
    } else {
        if (S_W_M === "inch") {
            S_W = S_W * 0.0254;
        } else {
            if (S_W_M === "yard") {
                S_W = S_W * 0.9144;
            } else {
                if (S_W_M === "centimeter") {
                    S_W = S_W * 0.01;
                }
            }
        }
    }
    
    if (S_H_M === "foot") {
        S_H = S_H * 0.3048;
    } else {
        if (S_H_M === "inch") {
            S_H = S_H * 0.0254;
        } else {
            if (S_H_M === "yard") {
                S_H = S_H * 0.9144;
            } else {
                if (S_H_M === "centimeter") {
                    S_H = S_H * 0.01;
                }
            }
        }
    }
    
    EQ = (S_L * S_W * S_H )* S_Q;
    
    $('#sb-total').html(EQ.toFixed(2));
  });

});

// Calculator for Col / Piles
jQuery(document).ready(function($) {
$( ".form-control" )
  .change(function () {
    var C_D_M = $("#c-d-unit").val();
    var C_H_M = $("#c-h-unit").val();
    
    var C_D = $("#c-Diameter").val();
    var C_H = $("#c-Height").val();
    
    var C_Q = $("#c-Quantity").val();
    
    if (C_D_M === "foot") {
          C_D = C_D * 0.3048;
        } else if (C_D_M === "inch") {
          C_D = C_D * 0.0254;
        } else if (C_D_M === "yard") {
          C_D = C_D * 0.9144;
        } else if (C_D_M === "centimeter") {
          C_D = C_D * 0.01;
      	}

      if (C_H_M === "foot") {
        C_H = C_H * 0.3048;
      } else if (C_H_M === "inch") {
        C_H = C_H * 0.0254;
      } else if (C_H_M === "yard") {
        C_H = C_H * 0.9144;
      } else if (C_H_M === "centimeter") {
        C_H = C_H * 0.01;
      }

    ECQZ = C_D / 2;

    //ECQ = (ECQZ * ECQZ) * C_H * 3.14159;

    fv1 = 3.14 * C_H * Math.pow(ECQZ,2);

    //ECQ = C_H * 3.14159 * (C_D * C_D) * C_Q / 4;
    
    $('#cd-total').html(fv1.toFixed(2));
  });
});



// Calculator for Slabs
jQuery(document).ready(function($) {

$( ".form-control" )
  .change(function () {
    var CS_OD_M = $("#cs-od-unit").val();
    var CS_ID_M = $("#cs-id-unit").val();
    var CS_H_M = $("#cs-h-unit").val();
    
    var CS_OD = $("#cs-ODiameter").val();
    var CS_ID = $("#cs-IDiameter").val();
    var CS_H = $("#cs-Height").val();
    
    var CS_Q = $("#cs-Quantity").val();
    
    if (CS_OD_M === "foot") {
        CS_OD = CS_OD * 0.3048;
    } else {
        if (CS_OD_M === "inch") {
            CS_OD = CS_OD * 0.0254;
        } else {
            if (CS_OD_M === "yard") {
                CS_OD = CS_OD * 0.9144;
            } else {
                if (CS_OD_M === "centimeter") {
                    CS_OD = CS_OD * 0.01;
                }
            }
        }
    }
    
    if (CS_ID_M === "foot") {
        CS_ID = CS_ID * 0.3048;
    } else {
        if (CS_ID_M === "inch") {
            CS_ID = CS_ID * 0.0254;
        } else {
            if (CS_ID_M === "yard") {
                CS_ID = CS_ID * 0.9144;
            } else {
                if (CS_ID_M === "centimeter") {
                    CS_ID = CS_ID * 0.01;
                }
            }
        }
    }
    
    if (CS_H_M === "foot") {
        CS_H = CS_H * 0.3048;
    } else {
        if (CS_H_M === "inch") {
            CS_H = CS_H * 0.0254;
        } else {
            if (CS_H_M === "yard") {
                CS_H = CS_H * 0.9144;
            } else {
                if (CS_H_M === "centimeter") {
                    CS_H = CS_H * 0.01;
                }
            }
        }
    }
    
    CSEQ = CS_H * 3.14159 * ((CS_OD * CS_OD) - (CS_ID * CS_ID)) * CS_Q / 4;
    
    $('#cs-total').html(CSEQ.toFixed(2));
  });

});


